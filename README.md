**examples for docker-compose.**

```yml
docker network create traefik-net

cd gle-traefik/
docker-compose -f docker-compose.traefik.yml up -d

cd ../gle-dokuwiki/
docker-compose up -d

cd ../gle-nextcloud/
docker-compose up -d

cd ../gle-zabbix/
docker-compose -f docker-compose_v3_alpine_mysql_latest_traefik.yaml up -d

cd ../gle-syncthing/
docker-compose up -d

```